package com.enterprise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.ComponentScan;



@EnableEurekaServer
@SpringBootConfiguration
@ComponentScan
@EnableAutoConfiguration
public class MicroServiceRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroServiceRegistryApplication.class, args);
	}
}
