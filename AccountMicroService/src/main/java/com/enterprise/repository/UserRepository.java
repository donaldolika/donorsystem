package com.enterprise.repository;

import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.enterprise.entities.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>{

	public List<User> findAll();

	public User findOne(long id);

	public void delete(long id);

	public User findByUsername(String username);
	
	public boolean exists(Long id);
}
