package com.enterprise.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class PaginationUtil {

	public static Pageable createPageRequest(int page, int size) {
		return new PageRequest(page - 1, size);
	}

}
