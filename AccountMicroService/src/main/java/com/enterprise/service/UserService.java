package com.enterprise.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.enterprise.entities.User;

public interface UserService {

	public List<User> findAll();

	public void saveUser(User user);

	public User findOne(long id);

	public void delete(long id);

	public User findByUsername(String username);

	public boolean exists(Long id);

	public Page<User> findAll(int page, int size);

}
