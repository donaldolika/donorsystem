package com.enterprise.serviceimp;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.enterprise.entities.User;
import com.enterprise.repository.UserRepository;
import com.enterprise.service.UserService;
import com.enterprise.util.PaginationUtil;

@Service
@Transactional
public class UserServiceImpl implements UserService, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public void saveUser(User user) {
		userRepository.save(user);
	}

	@Override
	public Page<User> findAll(int page, int size) {
		return userRepository.findAll(PaginationUtil.createPageRequest(page, size));
	}

	@Override
	public User findOne(long id) {
		return userRepository.findOne(id);
	}

	@Override
	public void delete(long id) {
		userRepository.delete(id);
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public boolean exists(Long id) {
		return userRepository.exists(id);
	}
}
