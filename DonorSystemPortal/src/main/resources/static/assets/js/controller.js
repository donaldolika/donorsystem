var app = angular.module("donorApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "index.html"
    })
    .when("/login", {
        templateUrl : "index.html"
    })
    .when("/dashboard", {
        templateUrl : "dashboard.html"
    })
});