var donations=new Array();
var bloodTypes=["A-", "A+", "B-","B+","O-","O+","AB-","AB+"];
'use strict';
  
angular.module('Donation').controller('donationController',
   ['$scope', '$rootScope','$uibModal', '$log', '$document',
    function ($scope, $rootScope,$uibModal, $log, $document) {
        $scope.username=$rootScope.globals.currentUser.username;
        $scope.branch=$rootScope.globals.currentUser.branch;
        $scope.qrcodeString=generateUUID();
        $scope.isGenerated=false;
        $scope.size = 250;
        $scope.correctionLevel = '';
        $scope.typeNumber = 0;
        $scope.inputMode = '';
        $scope.image = true;
        var $ctrl = this;
        $ctrl.animationsEnabled = true;

        $ctrl.open = function (size, parentSelector) {
          var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
          var modalInstance = $uibModal.open({
            animation: $ctrl.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'modules/donorList/views/newDonor.html',
            controller: 'ModalInstanceCtrl',
            controllerAs: '$ctrl',
            size: 'lg',
            appendTo: parentElem,
            resolve: {
              items: function () {
                return $ctrl.items;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            $ctrl.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };

        $ctrl.toggleAnimation = function () {
          $ctrl.animationsEnabled = !$ctrl.animationsEnabled;
        };

       $scope.change=function(){
       //check for length first
       if($scope.bloodDonorPersonalNumber.length==10)
       {

        //check if donor exists
          if('J123456789'==$scope.bloodDonorPersonalNumber)
        {
          //add donor data
          $scope.bloodTypeDonated="A+";
        }
          else
          {
          //create pop up to create new donor
          $scope.bloodTypeDonated="";
          $ctrl.open();          
          }
        }
       }                    ;

       $scope.submitForm = function(isValid) {
        // check to make sure the form is completely valid
        if (isValid) {
                 // $ctrl.open(); 
                 donations.push(
                  {
                    donorPersonalNumber:$scope.bloodDonorPersonalNumber,
                    donorBloodType:$scope.bloodTypeDonated,
                    donationDate:$scope.dateBirth,
                    donationBranch:$rootScope.globals.currentUser.branch,
                    qrCode:$scope.qrcodeString
                  });
                 alert(JSON.stringify(donations[0]));
        }
      };

      $scope.generateQRCode=function()
      {
        //make QR Code Appear
        $scope.isGenerated=true;
      }

      $scope.resetDonationForm=function()
      {
        $scope.bloodDonorPersonalNumber="";
        $scope.bloodTypeDonated="";
        $scope.dateBirth="";
        $scope.isValid=false;
        $scope.qrcodeString=generateUUID();
        $scope.isGenerated=false;
      }

      $scope.print=function()
      {
        //To DO Fix print
         window.print();
      }

    }]);


angular.module('Donation').controller('ModalInstanceCtrl',['$rootScope','$scope', function ($scope,$rootScope,$uibModalInstance) {
  
  $scope.donorBloodType = bloodTypes;
  $scope.cities=["Elbasan","Tirana","Vlora","Fier","Sarande"];
  $scope.branch="Tirane";

  $scope.submitNewDonorForm=function(isValid)
  {
    if(isValid)
    {

     // var user={
     //    name:$scope.donorName,
     //      lastname:$scope.donorLastName,
     //      donorPersonalNumber:$scope.donorPersonalNumber,
     //      donorBloodType:$scope.selectedDonorBloodType,
     //      city:$scope.selectedDonorCity,
     //      donorAddress:$scope.donorAddress,
     //      email:$scope.email,
     //      donorPhoneNumber:$scope.donorPhoneNumber,
     //      branchID:$scope.branchID,
     //      dateBirth:$scope.branch
     // };
      alert($scope.donorPersonalNumber);
    }
  }
}]);

function generateUUID(){
    var d = new Date().getTime();
    if(window.performance && typeof window.performance.now === "function"){
        d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
}