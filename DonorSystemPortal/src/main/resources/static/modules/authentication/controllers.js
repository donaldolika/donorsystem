'use strict';
 
angular.module('Authentication')
 
.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
        // reset login status
        AuthenticationService.ClearCredentials();
 
        $scope.login = function () {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function(response) {
                if(response.success) {
                    if($scope.username=="test")
                    AuthenticationService.SetCredentials($scope.username, $scope.password,'nurse','Tirana');
                    else if($scope.username=="admin")
                   {
                        AuthenticationService.SetCredentials($scope.username, $scope.password,'admin','Tirana');
                   }
                   else
                   {
                        AuthenticationService.SetCredentials($scope.username, $scope.password,'sysadmin','Tirana');
                   }

                    $location.path('/');
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });
        };
    }]);