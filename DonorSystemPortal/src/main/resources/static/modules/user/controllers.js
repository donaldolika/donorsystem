'use strict';
 
angular.module('User')

.controller('UserController',
   ['$scope', '$rootScope',
    function ($scope, $rootScope) {
        $scope.username=$rootScope.globals.currentUser.username;
        $scope.userName=$rootScope.globals.currentUser.username;
        $scope.userRole=$rootScope.globals.currentUser.role;
        $scope.branchID=$rootScope.globals.currentUser.branch;
		$scope.updateUserProfile = function () {
			
            //call service method and use input as parameters
			var user={
             username:$scope.username,
             password:$scope.password,
             age:$scope.userAge,
             firstName:$scope.firstName,
             lastName:$scope.lastName,
             email:$scope.email,
             userRole:$scope.userRole,
             branchID:$scope.branchID,
             userPhoneNumber:$scope.userPhoneNumber
			};

			alert(JSON.stringify(user));
			
        };
		
		
    }]);