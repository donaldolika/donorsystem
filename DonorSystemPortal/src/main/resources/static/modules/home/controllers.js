'use strict';
 
angular.module('Home')
 
.controller('HomeController',
   ['$scope', '$rootScope',
    function ($scope, $rootScope) {
        $scope.username=$rootScope.globals.currentUser.username;
        $rootScope.globals.currentUser.username;
        //isSysAdmin=false, isAdmin=false, isNurse=false;
        if($rootScope.globals.currentUser.role=="nurse")
        {
        	$rootScope.isNurse=true;$rootScope.isAdmin=false; $rootScope.isSysAdmin=false;
        }
        else if($rootScope.globals.currentUser.role=="admin")
        {
        	$rootScope.isNurse=false;$rootScope.isAdmin=true; $rootScope.isSysAdmin=false;
        }
        else
        {
        	$rootScope.isNurse=false;$rootScope.isAdmin=false; $rootScope.isSysAdmin=true;
        }

    }]);