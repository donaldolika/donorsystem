'use strict';
//global variables
//var isSysAdmin=false, isAdmin=false, isNurse=false;
// declare modules
angular.module('Authentication', []);
angular.module('Home', []);
angular.module('User',[]);
angular.module('Notify',[]);
angular.module('Donation',['ngAnimate', 'ngSanitize', 'ui.bootstrap','ja.qr']);
angular.module('UserList',['ngAnimate', 'ngTouch', 'ui.grid','ngSanitize', 'ui.bootstrap']);
angular.module('BranchList',[]);
angular.module('DonorList',[]);
angular.module('BasicHttpAuthExample', [
    'Authentication',
    'Home',
	'User',
	'Notify',
	'Donation',
	'UserList',
    'DonorList',
    'ngRoute',
    'ngCookies'
])
 
.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'modules/authentication/views/login.html',
            hideMenus: true
        })
		
		 .when('/user', {
            controller: 'UserController',
            templateUrl: 'modules/user/views/user.html'
        })
		
		.when('/notify', {
            controller: 'notifyController',
            templateUrl: 'modules/notify/views/notification.html'
        })
		
		.when('/userList', {
            controller: 'userListController',
            templateUrl: 'modules/userList/views/userList.html'
        })

        .when('/branchList', {
            controller: 'branchListController',
            templateUrl: 'modules/branchList/views/branchList.html'
        })

        .when('/donorList', {
            controller: 'donorListController',
            templateUrl: 'modules/donorList/views/donorList.html'
        })
 
        .when('/donation', {
            controller: 'donationController',
            templateUrl: 'modules/donation/views/donation.html'
        })

        .when('/', {
            controller: 'HomeController',
            templateUrl: 'modules/home/views/dashboard.html'
        })
		
        .otherwise({ redirectTo: '/login' });
}])
 
.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
 
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);