package com.enterprise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
public class DonorSystemPortalApplication {

	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(DonorSystemPortalApplication.class, args);
	}
}
