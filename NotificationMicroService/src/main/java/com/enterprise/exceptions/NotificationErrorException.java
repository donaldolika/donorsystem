package com.enterprise.exceptions;

import com.enterprise.utilities.Util;
import com.twilio.sdk.TwilioRestException;

public class NotificationErrorException extends TwilioRestException {

	public NotificationErrorException(String message, int errorCode) {
		super(Util.smsErrorMessage(message), errorCode);
	}

	private static final long serialVersionUID = 1L;

}
