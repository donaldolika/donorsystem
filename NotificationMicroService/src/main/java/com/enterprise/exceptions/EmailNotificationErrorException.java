package com.enterprise.exceptions;

import com.enterprise.utilities.Util;

public class EmailNotificationErrorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmailNotificationErrorException() {
		super();
	}

	public EmailNotificationErrorException(String message) {
		super(Util.emailErrorMessage(message));
	}

}
