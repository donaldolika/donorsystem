package com.enterprise.service;

import java.util.List;

import com.enterprise.domain.Recipient;
import com.enterprise.exceptions.NotificationErrorException;

public interface SmsService {

	void send(Recipient recipient);
	
	void notifyArea(List<Recipient> recipients);

}
