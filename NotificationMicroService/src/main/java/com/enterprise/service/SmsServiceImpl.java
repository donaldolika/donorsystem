package com.enterprise.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.enterprise.domain.NotificationType;
import com.enterprise.domain.Recipient;
import com.enterprise.exceptions.NotificationErrorException;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;

@Service
public class SmsServiceImpl implements SmsService {

	@Autowired
	private TwilioRestClient twilioRestClient;

	@Autowired
	private Environment env;

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final int PERSONNOTIFY = 1;
	private static final int AREANOTIFY = 2;

	public void sendNon(Recipient recipient) throws NotificationErrorException {
		try {
			twilioRestClient.getAccount().getMessageFactory().create(processMessage(PERSONNOTIFY, recipient));
			log.debug("SMS notification has been send to {}", recipient.getPhoneNumber());
		} catch (TwilioRestException e) {
			throw new NotificationErrorException(e.getErrorMessage(), e.getErrorCode());
		}

	}

	public void notifyAreaNon(List<Recipient> recipients) throws NotificationErrorException {
		for (Recipient recipient : recipients) {
			try {
				twilioRestClient.getAccount().getMessageFactory().create(processMessage(AREANOTIFY, recipient));
			} catch (TwilioRestException e) {
				throw new NotificationErrorException(e.getErrorMessage(), e.getErrorCode());
			}
		}
		log.info("SMS notification has been send to {} recipients", recipients.size());

	}

	
	//ACYNCMethods
	
	@Override
	public void send(Recipient recipient) {
		CompletableFuture.runAsync(() -> {
			try {
				twilioRestClient.getAccount().getMessageFactory().create(processMessage(PERSONNOTIFY, recipient));
				log.debug("SMS notification has been send to {}", recipient.getPhoneNumber());
			} catch (TwilioRestException e) {
				log.error("Error during Area SMS notification for {}", recipient);
			}
		});
	}

	@Override
	public void notifyArea(List<Recipient> recipients) {
		recipients.forEach(recipient -> CompletableFuture.runAsync(() -> {
			try {
				twilioRestClient.getAccount().getMessageFactory().create(processMessage(AREANOTIFY, recipient));
			} catch (TwilioRestException e) {
				log.error("Error during Area SMS notification for {}", recipient);
			}
		}));
		log.info("SMS notification has been send to {} recipients", recipients.size());
	}

	private List<NameValuePair> processMessage(int type, Recipient recipient) {
		log.info("PROCESSING MESSAGE");
		NotificationType notificationType = new NotificationType();
		switch (type) {
		case 1:
			notificationType.setText(MessageFormat.format(env.getProperty("phone.perdorim.text"),
					recipient.getAccountName(), new Date().toString()));
			notificationType.setSubject(env.getProperty("phone.perdorim.subject"));
			notificationType.setAttachment(env.getProperty("phone.perdorim.attachment"));

			break;

		case 2:
			notificationType
					.setText(MessageFormat.format(env.getProperty("phone.lajmerim.text"), recipient.getAccountName()));
			notificationType.setSubject(env.getProperty("phone.lajmerim.subject"));
			notificationType.setAttachment(env.getProperty("phone.lajmerim.attachment"));
			break;
		}
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("To", recipient.getPhoneNumber()));
		params.add(new BasicNameValuePair("From", env.getProperty("phone.number")));
		params.add(new BasicNameValuePair("Body", notificationType.getText()));

		return params;

	}

}
