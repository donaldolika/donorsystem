package com.enterprise.service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.enterprise.domain.NotificationType;
import com.enterprise.domain.Recipient;
import com.enterprise.exceptions.EmailNotificationErrorException;

@Service
public class EmailServiceImpl implements EmailService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private Environment env;

	private static final int PERSONNOTIFY = 1;
	private static final int AREANOTIFY = 2;

	
	//ASYNCSRON METHODS
	
	
	@Override
	public void send(Recipient recipient) {
		CompletableFuture.runAsync(() -> {
			try {
				javaMailSender.send(processMessage(javaMailSender.createMimeMessage(), PERSONNOTIFY, recipient));
			} catch (MailException | EmailNotificationErrorException e) {
			}
			log.info("Email notification has been send to {}", recipient.getEmail());
		});
	}

	@Override
	public void notifyArea(List<Recipient> recipients) {
		recipients.forEach(recipient -> CompletableFuture.runAsync(() -> {
			try {
				javaMailSender.send(processMessage(javaMailSender.createMimeMessage(), AREANOTIFY, recipient));
			} catch (MailException | EmailNotificationErrorException e) {
				log.error("Error during Area notification for {}", recipient);
			}

		}));
		log.info("Email notification has been send to {} recipients", recipients.size());
	}
	
	//NON ASYNCRON METHOD
	public void sendNon(Recipient recipient) throws EmailNotificationErrorException {
		javaMailSender.send(processMessage(javaMailSender.createMimeMessage(), PERSONNOTIFY, recipient));
		log.info("Email notification has been send to {}", recipient.getEmail());
	}


	public void notifyAreaNon(List<Recipient> recipients) throws EmailNotificationErrorException {
		for (Recipient recipient : recipients) {
			javaMailSender.send(processMessage(javaMailSender.createMimeMessage(), AREANOTIFY, recipient));
		}
		log.info("Email notification has been send to {} recipients", recipients.size());
	}
	
	
	
	

	private MimeMessage processMessage(MimeMessage message, int type, Recipient recipient)
			throws EmailNotificationErrorException {
		NotificationType notificationType = new NotificationType();
		switch (type) {
		case 1:
			notificationType.setText(MessageFormat.format(env.getProperty("email.perdorim.text"),
					recipient.getAccountName(), new Date().toString()));
			notificationType.setSubject(env.getProperty("email.perdorim.subject"));
			notificationType.setAttachment(env.getProperty("email.perdorim.attachment"));
			break;

		case 2:
			notificationType
					.setText(MessageFormat.format(env.getProperty("email.lajmerim.text"), recipient.getAccountName()));
			notificationType.setSubject(env.getProperty("email.lajmerim.subject"));
			notificationType.setAttachment(env.getProperty("email.lajmerim.attachment"));
			break;
		}

		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(message, true);
			helper.setTo(recipient.getEmail());
			helper.setSubject(notificationType.getSubject());
			helper.setText(notificationType.getText());
		} catch (MessagingException e) {
			throw new EmailNotificationErrorException(e.getMessage());
		}
		return message;

	}
}


	