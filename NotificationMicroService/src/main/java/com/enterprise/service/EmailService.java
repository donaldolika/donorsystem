package com.enterprise.service;

import java.util.List;

import com.enterprise.domain.Recipient;
import com.enterprise.exceptions.EmailNotificationErrorException;

public interface EmailService {

	void send(Recipient recipient);

	void notifyArea(List<Recipient> recipients);

}
