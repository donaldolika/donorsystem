package com.enterprise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.twilio.sdk.TwilioRestClient;

@PropertySource("classpath:applicationtexts.properties")
@SpringBootApplication
public class NotificationMicroServiceApplication {

	@Autowired
	Environment environment;

	public static void main(String[] args) {
		SpringApplication.run(NotificationMicroServiceApplication.class, args);
	}

	@Bean
	public TwilioRestClient getTwilioRestClient() {
		return new TwilioRestClient(environment.getProperty("twilio.username"),
				environment.getProperty("twilio.password"));
	}
}
