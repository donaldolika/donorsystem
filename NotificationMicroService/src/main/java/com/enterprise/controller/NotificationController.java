package com.enterprise.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.enterprise.domain.Recipient;
import com.enterprise.service.EmailService;
import com.enterprise.service.SmsService;

@RestController
@RequestMapping("/")
public class NotificationController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SmsService smsService;

	@Autowired
	private EmailService emailService;
	
	//TODO NEED TO BE FIXED ,Future OBJECT TO BE RETURNED

	@PreAuthorize("#oauth2.hasScope('server')")
	@RequestMapping(value = "/message/", method = RequestMethod.POST)
	public ResponseEntity<Void> emailnotification(@RequestBody Recipient recipient) {
		logger.debug("Sending Email Notification to {}", recipient.getEmail());
		emailService.send(recipient);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@PreAuthorize("#oauth2.hasScope('server')")
	@RequestMapping(value = "/message/area", method = RequestMethod.POST)
	public ResponseEntity<Void> emailnotifications(@RequestBody List<Recipient> recipients) {
		logger.debug("Sending Email Notifications to {}", recipients.size());
		emailService.notifyArea(recipients);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@PreAuthorize("#oauth2.hasScope('server')")
	@RequestMapping(value = "/sms/", method = RequestMethod.POST)
	public ResponseEntity<Void> smsnotification(@RequestBody Recipient recipient) {
		smsService.send(recipient);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}


	@PreAuthorize("#oauth2.hasScope('server')")
	@RequestMapping(value = "/sms/area", method = RequestMethod.POST)
	public ResponseEntity<Void> smsnotifications(@RequestBody List<Recipient> recipients) {
		smsService.notifyArea(recipients);
		return new ResponseEntity<Void>(HttpStatus.OK);

	}

}
