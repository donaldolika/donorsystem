package com.enterprise.domain;

public class Donor {

	private String name;

	private String lastname;

	private String phoneNumber;

	private String email;

	private String address;

	private String bllodType;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBllodType() {
		return bllodType;
	}

	public void setBllodType(String bllodType) {
		this.bllodType = bllodType;
	}

}
