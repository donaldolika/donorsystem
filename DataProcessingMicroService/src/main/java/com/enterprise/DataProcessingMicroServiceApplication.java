package com.enterprise;

import org.aspectj.apache.bcel.generic.ReturnaddressType;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
public class DataProcessingMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataProcessingMicroServiceApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
