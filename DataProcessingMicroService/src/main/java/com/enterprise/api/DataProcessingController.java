package com.enterprise.api;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enterprise.client.DonorsDataClient;

import com.enterprise.client.NotificationDataClient;
import com.enterprise.domain.Donor;
import com.enterprise.domain.Message;
import com.enterprise.service.BarcodeToMessageService;

@RestController
@RequestMapping("/")
public class DataProcessingController {

	@Autowired
	NotificationDataClient notificationDataClient;

	@Autowired
	DonorsDataClient donorsDataClient;

	@Autowired
	BarcodeToMessageService barcodeToMessageService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/areaNotify", method = RequestMethod.POST)
	public ResponseEntity<Void> notifyArea(@RequestParam("city") String city, @RequestParam("type") String type) {
		logger.info("Starting to Notify Donor");
		List<Donor> donors = donorsDataClient.notifyList(city, type);
		donors.forEach(donor -> CompletableFuture.runAsync(() -> {
			Message message = new Message();
			message.setAccountName(donor.getName());
			message.setEmail(donor.getEmail());
			message.setPhoneNumber(donor.getPhoneNumber());
			notificationDataClient.notifyUser(message);
		}));
		logger.info("Notified Donors", donors.size());
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(value = "/data/", method = RequestMethod.POST)
	public ResponseEntity<Void> notifyDonor(@RequestParam("code") String code) {
		logger.info("Starting to Notify Donor");
		Message message = barcodeToMessageService.convertToMessage(code);
		notificationDataClient.notifyUser(message);
		logger.info("Ended notifing Donor");
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	// TODO DONATION NEED TO BE ADDED ON ENCRYPTION

	@RequestMapping(value = "/generate/", method = RequestMethod.POST)
	public ResponseEntity<String> generateBarCode(@RequestBody Donor donor) {
		logger.info("Starting to generateBarCode");
		String generated;
		try {
			generated = barcodeToMessageService.barCodeGeneration(donor, null);
			logger.info("Ending to generateBarCode");
			return new ResponseEntity<String>(generated, HttpStatus.OK);
		} catch (IOException | InterruptedException e) {
			logger.error("Failed Generating BarCode");
			return new ResponseEntity<String>(HttpStatus.CONFLICT);
		}

	}

}
