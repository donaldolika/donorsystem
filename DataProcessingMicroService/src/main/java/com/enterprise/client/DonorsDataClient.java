package com.enterprise.client;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.enterprise.domain.Donor;

//service just for test need to be fixed + ASYNC METHOD

@Service
public class DonorsDataClient {

	@Autowired
	RestTemplate restTemplate;

	// TODO OAuth2RestOperations NEED TO BE CALLED ON REQUESTS

	public List<Donor> notifyList(String city, String bloodType) {
		Donor[] donors = restTemplate.getForObject("http://localhost:8075/donor/", Donor[].class);
		List<Donor> lista = Arrays.asList(donors);
		return lista;
	}

}
