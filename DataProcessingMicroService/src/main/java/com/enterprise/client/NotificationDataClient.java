package com.enterprise.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.enterprise.domain.Message;

//TODO service just for test need to be fixed + ASYNC METHOD

@Service
public class NotificationDataClient {


	@Autowired
	RestTemplate restTemplate;

	//TODO OAuth2RestOperations NEED TO BE CALLED ON REQUESTS

	public void notifyList(List<Message> messages) {
		restTemplate.postForLocation("http://localhost:8090/message/area", messages);
	}

	public void notifyUser(Message message) {
		restTemplate.postForLocation("http://localhost:8090/message/", message);
	}

}
