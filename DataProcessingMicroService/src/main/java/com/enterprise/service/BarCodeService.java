package com.enterprise.service;

import java.io.IOException;

public interface BarCodeService {

	public String barcodeAsBase64(String code) throws IOException, InterruptedException;
	
	public String qrCodeAsBase64(String code) throws IOException, InterruptedException;

}
