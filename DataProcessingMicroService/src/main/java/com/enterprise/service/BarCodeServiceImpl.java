package com.enterprise.service;

import net.glxn.qrgen.QRCode;
import org.krysalis.barcode4j.impl.AbstractBarcodeBean;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.enterprise.config.BarcodeConfig;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Service
public class BarCodeServiceImpl implements BarCodeService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private byte[] barcode(String barcode, BarcodeConfig.Barcode barcodeConfig) throws IOException {
		AbstractBarcodeBean codeBean = new Code128Bean();

		final int dpi = barcodeConfig.getDpi();

		codeBean.setHeight(barcodeConfig.getHeight());
		codeBean.setBarHeight(barcodeConfig.getBarHeight());
		codeBean.setFontSize(barcodeConfig.getFontSize());
		codeBean.setModuleWidth(barcodeConfig.getModuleWidth());

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		BitmapCanvasProvider canvasProvider = new BitmapCanvasProvider(byteArrayOutputStream, "image/x-png", dpi,
				BufferedImage.TYPE_BYTE_BINARY, false, 0);
		codeBean.generateBarcode(canvasProvider, barcode);
		canvasProvider.finish();

		return byteArrayOutputStream.toByteArray();
	}

	private BufferedImage barcode(String barcode) throws IOException {
		byte[] bytes = barcode(barcode, BarcodeConfig.Barcode.defaultConfig());
		try {
			return ImageIO.read(new ByteArrayInputStream(bytes));
		} catch (IOException e) {
			log.error("Error barcode to BufferImage");
		}
		return null;
	}

	@Override
	public String barcodeAsBase64(String code) throws IOException, InterruptedException {
		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		BufferedImage bufferedImage = barcode(code);
		ImageIO.write(bufferedImage, "png", Base64.getEncoder().wrap(os));
		return os.toString(StandardCharsets.UTF_8.name());
	}

	private byte[] qrCode(String code, BarcodeConfig.QRCode qrCodeConfig) throws InterruptedException, IOException {
		return QRCode.from(code).withSize(qrCodeConfig.getWidth(), qrCodeConfig.getHeight())
				.to(qrCodeConfig.getImageType()).stream().toByteArray();
	}

	private BufferedImage qrCode(String code) throws InterruptedException, IOException {
		byte[] bytes = qrCode(code, BarcodeConfig.QRCode.defaultConfig());
		try {
			return ImageIO.read(new ByteArrayInputStream(bytes));
		} catch (IOException e) {
			log.error("Error qrcode to BufferImage");
		}
		return null;
	}

	@Override
	public String qrCodeAsBase64(String code) throws IOException, InterruptedException {
		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		BufferedImage bufferedImage = qrCode(code);
		ImageIO.write(bufferedImage, "png", Base64.getEncoder().wrap(os));
		return os.toString(StandardCharsets.UTF_8.name());
	}
}
