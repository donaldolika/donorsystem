package com.enterprise.service;


public interface EncryptionService {
	
	public String decrypt(String strToDecrypt);
	
	public String encrypt(String strToEncrypt);
	
}
