package com.enterprise.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EncryptionServiceImpl implements EncryptionService {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	
	@Value("encryption.pattern.key")
	private String encryptionKey;
	
	private SecretKeySpec secretKey;
	

	// TODO SET KEY need to be added from encryptionKey
	private byte[] key = setKey("mekey");
	private String decryptedString;
	private String encryptedString;

	private byte[] setKey(String myKey) {
		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");
			return key;
		} catch (NoSuchAlgorithmException e) {

		} catch (UnsupportedEncodingException e) {
		}
		log.error("Error Setting Encryption Key");
		return null;
	}

	private void setDecryptedString(String decryptedString) {
		this.decryptedString = decryptedString;
	}

	private void setEncryptedString(String encryptedString) {
		this.encryptedString = encryptedString;
	}

	public String encrypt(String strToEncrypt) {
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			setEncryptedString(Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes("UTF-8"))));
			return encryptedString;
		} catch (Exception e) {
			log.error("Error while encrypting: ", e.toString());
		}
		return null;
	}

	public String decrypt(String strToDecrypt) {
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			setDecryptedString(new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt))));
			return decryptedString;
		} catch (Exception e) {
			log.error("Error while decrypting: " + e.toString());
		}
		return null;
	}

}
