package com.enterprise.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enterprise.domain.Donation;
import com.enterprise.domain.Donor;
import com.enterprise.domain.Message;

@Service
public class BarCodeToMessageServiceImpl implements BarcodeToMessageService {

	@Autowired
	BarCodeService barCodeService;

	@Autowired
	EncryptionService encryptionService;

	@Override
	public Message convertToMessage(String code) {
		String decrypted = encryptionService.decrypt(code);
		return parseStringtoMessage(decrypted);
	}

	@Override
	public String barCodeGeneration(Donor donor, Donation donation) throws IOException, InterruptedException {
		String encrypted = convertToCode(donor, donation);
		return barCodeService.barcodeAsBase64(encrypted);
	}

	// TODO PATTER ALGORITHM NEED TO BE FIXED

	private String convertToCode(Donor donor, Donation donation) {
		String convertedString = donor.getName() + "--" + donor.getBllodType() + "--" + donor.getAddress() + "--"
				+ donor.getEmail() + "--" + donor.getPhoneNumber();
		return encryptionService.encrypt(convertedString);
	}

	// TODO Parsing Pattern need to be Fixed

	private Donor parseStringtoDonor(String code) {
		Donor donor = new Donor();
		String[] codesplitet = code.split("--");
		donor.setName(codesplitet[0]);
		donor.setBllodType(codesplitet[1]);
		donor.setAddress(codesplitet[2]);
		donor.setEmail(codesplitet[3]);
		donor.setPhoneNumber(codesplitet[4]);
		return donor;
	}

	private Message parseStringtoMessage(String code) {
		Message message = new Message();
		String[] codesplitet = code.split("--");
		message.setAccountName(codesplitet[0]);
		message.setEmail(codesplitet[3]);
		message.setPhoneNumber(codesplitet[4]);
		return message;
	}

}
