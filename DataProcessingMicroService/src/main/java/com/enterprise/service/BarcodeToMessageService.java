package com.enterprise.service;

import java.io.IOException;

import com.enterprise.domain.Donation;
import com.enterprise.domain.Donor;
import com.enterprise.domain.Message;

public interface BarcodeToMessageService {

	public Message convertToMessage(String code);

	public String barCodeGeneration(Donor donor, Donation donation) throws IOException, InterruptedException;

}
