package com.enterprise.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.enterprise.entity.Center;
import com.enterprise.entity.Donation;
import com.enterprise.entity.Donor;

public class PaginationUtil {

	public static Pageable createPageRequest(int first, int size, String sort, String sortField) {
		Sort sortdirection = sort.equals("asc") ? new Sort(Sort.Direction.ASC, sortField)
				: new Sort(Sort.Direction.DESC, sortField);
		return new PageRequest(first, size, sortdirection);
	}

	public static List<Donor> convertDonorPageToList(Page<Donor> page) {
		if (page.getTotalElements() > 0) {
			List<Donor> lista = new ArrayList<>();
			page.forEach(x -> {
				if (x != null) {
					lista.add(x);
				}

			});
			return lista;
		}
		return Collections.emptyList();
	}
	
	public static List<Center> convertCenterPageToList(Page<Center> page) {
		if (page.getTotalElements() > 0) {
			List<Center> lista = new ArrayList<>();
			page.forEach(x -> {
				if (x != null) {
					lista.add(x);
				}

			});
			return lista;
		}
		return Collections.emptyList();
	}
	
	public static List<Donation> convertDonationPageToList(Page<Donation> page) {
		if (page.getTotalElements() > 0) {
			List<Donation> lista = new ArrayList<>();
			page.forEach(x -> {
				if (x != null) {
					lista.add(x);
				}

			});
			return lista;
		}
		return Collections.emptyList();
	}
	
	

}
