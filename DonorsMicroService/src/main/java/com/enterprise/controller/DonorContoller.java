package com.enterprise.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.enterprise.entity.Donor;
import com.enterprise.service.DonorService;

@RestController
@RequestMapping("/")
public class DonorContoller {

	@Autowired
	private DonorService donorService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/donor/", method = RequestMethod.GET)
	public ResponseEntity<List<Donor>> listAllDonors() {
		List<Donor> donors = donorService.findAllDonors();
		if (donors.isEmpty()) {
			return new ResponseEntity<List<Donor>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Donor>>(donors, HttpStatus.OK);
	}

	@RequestMapping(value = "/donor/", params = { "page", "size" }, method = RequestMethod.GET)
	public ResponseEntity<List<Donor>> lazypaginatedList(@RequestParam("page") int page, @RequestParam("size") int size,
			UriComponentsBuilder uriBuilder) {
		List<Donor> donors = donorService.findAllDonors(page, size, "asc", "idBloodDonor");
		if (donors.isEmpty()) {
			return new ResponseEntity<List<Donor>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Donor>>(donors, HttpStatus.OK);
	}

	@RequestMapping(value = "/donor/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Donor> getDonor(@PathVariable("id") long id) {
		Donor dto = donorService.findOneDonorById(id);
		if (dto == null) {
			logger.info("Donor with id {} not found", id);
			return new ResponseEntity<Donor>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Donor>(dto, HttpStatus.OK);
	}

	// check it
	@RequestMapping(value = "/donor/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> createDonor(@RequestBody Donor donor, UriComponentsBuilder ucBuilder) {
		logger.debug("Creating Donor with name {}", donor.getName());
		donorService.saveDonor(donor);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/donor/{id}").buildAndExpand(donor.getIdBloodDonor()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	// check it
	@RequestMapping(value = "/donor/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Donor> updateDonor(@PathVariable("id") long id, @RequestBody Donor donor) {
		logger.debug("Updating Donor with id {} ", id);
		if (donorService.existsDonors(id)) {
			donor.setIdBloodDonor((int) id);
			donorService.saveDonor(donor);
			logger.debug("Donor with id {} updated with success ", id);
			return new ResponseEntity<Donor>(donor, HttpStatus.OK);
		}
		logger.debug("Donor with id {} not found", id);
		return new ResponseEntity<Donor>(HttpStatus.NOT_FOUND);

	}

	// check it
	@RequestMapping(value = "/donor/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Donor> deleteDonor(@PathVariable("id") long id) {
		logger.debug("Fetching & Deleting Donor with id {}", id);
		if (donorService.existsDonors(id)) {
			donorService.deleteDonor(id);
			logger.debug("Fetching & Deleting Donor with id {} was successfull", id);
			return new ResponseEntity<Donor>(HttpStatus.NO_CONTENT);
		}
		logger.debug("Unable to delete. Donor with id {} not found", id);
		return new ResponseEntity<Donor>(HttpStatus.NOT_FOUND);
	}

}
