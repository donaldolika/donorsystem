package com.enterprise.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.enterprise.entity.Donation;
import com.enterprise.service.DonationService;

@RestController
@RequestMapping("/")
public class DonationController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DonationService donationService;

	@RequestMapping(value = "/donation/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	private ResponseEntity<List<Donation>> getListOfDonations() {
		List<Donation> donationsList = donationService.findAllDonation();
		if (donationsList.isEmpty()) {
			return new ResponseEntity<List<Donation>>(HttpStatus.NO_CONTENT);
		} else
			return new ResponseEntity<List<Donation>>(donationsList, HttpStatus.OK);
	}

	@RequestMapping(value = "/donation/", params = { "page", "size" }, method = RequestMethod.GET)
	public ResponseEntity<List<Donation>> lazypaginatedList(@RequestParam("page") int page,
			@RequestParam("size") int size, UriComponentsBuilder uriBuilder) {
		List<Donation> donations = donationService.findAllDonation(page, size, "asc", "idDonation");
		if (donations.isEmpty()) {
			return new ResponseEntity<List<Donation>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Donation>>(donations, HttpStatus.OK);
	}

	@RequestMapping(value = "/donation/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Donation> getDonationById(@PathVariable("id") long id) {
		Donation dto = donationService.findOneDonationById(id);
		if (dto == null) {
			logger.info("Donation with id {} not found", id);
			return new ResponseEntity<Donation>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Donation>(dto, HttpStatus.OK);
	}

	@RequestMapping(value = "/donation/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> createDonation(@RequestBody Donation donation, UriComponentsBuilder ucBuilder) {
		logger.debug("Creating Doantion with id {}", donation.getIdDonation());
		donationService.saveDonation(donation);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/donation/{id}").buildAndExpand(donation.getIdDonation()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/donatation/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Donation> updateDonation(@PathVariable("id") long id, @RequestBody Donation donation) {
		logger.debug("Updating Donation with id {} ", id);
		if (donationService.existsDonationById(id)) {
			donation.setIdDonation((int) id);
			donationService.saveDonation(donation);
			return new ResponseEntity<Donation>(donation, HttpStatus.OK);
		}
		logger.debug("Donation with id {} not found", id);
		return new ResponseEntity<Donation>(HttpStatus.NOT_FOUND);

	}

	@RequestMapping(value = "/donation/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Donation> deleteCenter(@PathVariable("id") long id) {
		logger.debug("Fetching & Deleting Donation with id {}", id);
		if (donationService.existsDonationById(id)) {
			donationService.deleteDonation(id);
			return new ResponseEntity<Donation>(HttpStatus.NO_CONTENT);
		}
		logger.debug("Unable to delete. Donation with id {} not found", id);
		return new ResponseEntity<Donation>(HttpStatus.NOT_FOUND);
	}
}
