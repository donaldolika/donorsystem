package com.enterprise.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.enterprise.entity.Center;
import com.enterprise.service.CentersService;

@RestController
@RequestMapping("/")
public class CenterController {

	@Autowired
	private CentersService centerService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/center/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Center>> listAllCenters() {
		List<Center> donors = centerService.findAllCenters();
		if (donors.isEmpty()) {
			return new ResponseEntity<List<Center>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Center>>(donors, HttpStatus.OK);
	}

	@RequestMapping(value = "/center/", params = { "page", "size" }, method = RequestMethod.GET)
	public ResponseEntity<List<Center>> lazypaginatedList(@RequestParam("page") int page,
			@RequestParam("size") int size, UriComponentsBuilder uriBuilder) {
		List<Center> centers = centerService.findAllCenters(page, size, "asc", "idCenter");
		if (centers.isEmpty()) {
			return new ResponseEntity<List<Center>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Center>>(centers, HttpStatus.OK);
	}

	@RequestMapping(value = "/center/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Center> getCenter(@PathVariable("id") int id) {
		Center dto = centerService.findOneCenterById(id);
		if (dto == null) {
			logger.info("Center with id {} not found", id);
			return new ResponseEntity<Center>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Center>(dto, HttpStatus.OK);
	}

	@RequestMapping(value = "/center/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> createCenter(@RequestBody Center center, UriComponentsBuilder ucBuilder) {
		logger.debug("Creating Center {}", center.getName());
		centerService.saveCenter(center);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/donor/{id}").buildAndExpand(center.getIdCenter()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/center/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Center> updateCenter(@PathVariable("id") int id, @RequestBody Center center) {
		logger.debug("Updating Center {} ", id);
		if (centerService.existsCenterById(id)) {
			center.setIdCenter((int) id);
			centerService.saveCenter(center);
			return new ResponseEntity<Center>(center, HttpStatus.OK);
		}
		logger.debug("Center with id {} not found", id);
		return new ResponseEntity<Center>(HttpStatus.NOT_FOUND);

	}

	@RequestMapping(value = "/center/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Center> deleteCenter(@PathVariable("id") int id) {
		logger.debug("Fetching & Deleting Center with id {}", id);
		if (centerService.existsCenterById(id)) {
			centerService.deleteCenter(id);
			return new ResponseEntity<Center>(HttpStatus.NO_CONTENT);
		}
		logger.debug("Unable to delete. Center with id {} not found", id);
		return new ResponseEntity<Center>(HttpStatus.NOT_FOUND);
	}

}
