package com.enterprise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
public class DonorsMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DonorsMicroServiceApplication.class, args);
	}
}
