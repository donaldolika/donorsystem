package com.enterprise.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Donation {

	@Id
	private int idDonation;

	private Donor donorId;

	private String bloodType;

	private String qrCode;

	private Date donatedDate;

	private Date usedDate;

	private Center donatedCenter;

	private User createdBy;

	private int validity;

	private int isAvailable;

	public int getIdDonation() {
		return idDonation;
	}

	public void setIdDonation(int idDonation) {
		this.idDonation = idDonation;
	}

	public Donor getDonorId() {
		return donorId;
	}

	public void setDonorId(Donor donorId) {
		this.donorId = donorId;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public Date getDonatedDate() {
		return donatedDate;
	}

	public void setDonatedDate(Date donatedDate) {
		this.donatedDate = donatedDate;
	}

	public Date getUsedDate() {
		return usedDate;
	}

	public void setUsedDate(Date usedDate) {
		this.usedDate = usedDate;
	}

	public Center getDonatedCenter() {
		return donatedCenter;
	}

	public void setDonatedCenter(Center donatedCenter) {
		this.donatedCenter = donatedCenter;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public int getValidity() {
		return validity;
	}

	public void setValidity(int validity) {
		this.validity = validity;
	}

	public int getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(int isAvailable) {
		this.isAvailable = isAvailable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bloodType == null) ? 0 : bloodType.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((donatedCenter == null) ? 0 : donatedCenter.hashCode());
		result = prime * result + ((donatedDate == null) ? 0 : donatedDate.hashCode());
		result = prime * result + ((donorId == null) ? 0 : donorId.hashCode());
		result = prime * result + idDonation;
		result = prime * result + isAvailable;
		result = prime * result + ((qrCode == null) ? 0 : qrCode.hashCode());
		result = prime * result + ((usedDate == null) ? 0 : usedDate.hashCode());
		result = prime * result + validity;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Donation other = (Donation) obj;
		if (bloodType == null) {
			if (other.bloodType != null)
				return false;
		} else if (!bloodType.equals(other.bloodType))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (donatedCenter == null) {
			if (other.donatedCenter != null)
				return false;
		} else if (!donatedCenter.equals(other.donatedCenter))
			return false;
		if (donatedDate == null) {
			if (other.donatedDate != null)
				return false;
		} else if (!donatedDate.equals(other.donatedDate))
			return false;
		if (donorId == null) {
			if (other.donorId != null)
				return false;
		} else if (!donorId.equals(other.donorId))
			return false;
		if (idDonation != other.idDonation)
			return false;
		if (isAvailable != other.isAvailable)
			return false;
		if (qrCode == null) {
			if (other.qrCode != null)
				return false;
		} else if (!qrCode.equals(other.qrCode))
			return false;
		if (usedDate == null) {
			if (other.usedDate != null)
				return false;
		} else if (!usedDate.equals(other.usedDate))
			return false;
		if (validity != other.validity)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Donation [idDonation=" + idDonation + ", donorId=" + donorId + ", bloodType=" + bloodType + ", qrCode="
				+ qrCode + ", donatedDate=" + donatedDate + ", usedDate=" + usedDate + ", donatedCenter="
				+ donatedCenter + ", createdBy=" + createdBy + ", validity=" + validity + ", isAvailable=" + isAvailable
				+ "]";
	}

}
