package com.enterprise.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.enterprise.entity.Donation;
import com.enterprise.entity.Donor;

public interface DonationRepository extends MongoRepository<Donation, Long> {

	public List<Donation> findByDonorId(Donor donorId);
}
