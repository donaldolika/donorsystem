package com.enterprise.repository;

import com.enterprise.entity.Donor;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DonorRepository extends MongoRepository<Donor, Long> {
	

}
