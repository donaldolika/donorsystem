package com.enterprise.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.enterprise.entity.Center;

@Repository
public interface CenterRepository extends MongoRepository<Center,Integer> {

}
