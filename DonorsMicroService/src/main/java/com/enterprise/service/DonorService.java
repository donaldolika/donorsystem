package com.enterprise.service;

import java.util.List;
import com.enterprise.entity.Donor;

public interface DonorService {
	
	public Donor findOneDonorById(Long id);

	public void deleteDonor(Donor donor);
	
	public void deleteDonor(Long id);
	
	public boolean existsDonors(Long id);

	public Donor saveDonor(Donor donor);

	public List<Donor> findAllDonors();

	public List<Donor> findAllDonors(int size,int first,String sort,String field);
	
	public Donor insertDonors(Donor donor);
	
}
