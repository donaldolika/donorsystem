package com.enterprise.service;

import java.util.List;

import com.enterprise.entity.Center;

public interface CentersService {

	public Center findOneCenterById(Integer id);

	public void deleteCenter(Center center);

	public void deleteCenter(Integer id);

	public boolean existsCenterById(Integer id);

	public Center saveCenter(Center center);

	public List<Center> findAllCenters();
	
	public List<Center> findAllCenters(int size,int first,String sort,String field);

	public Center insertCenter(Center center);

}
