package com.enterprise.service;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enterprise.entity.Center;
import com.enterprise.repository.CenterRepository;
import com.enterprise.utilities.PaginationUtil;

@Service
public class CentersServiceImpl implements CentersService, Serializable {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(CentersServiceImpl.class);

	@Autowired
	private CenterRepository centerRepository;

	@Override
	public Center findOneCenterById(Integer id) {
		logger.debug("Searching Center with id= {}", id);
		return centerRepository.findOne(id);
	}

	@Override
	public void deleteCenter(Center center) {
		logger.debug("Deleting Center {}", center);
		centerRepository.delete(center);
	}

	@Override
	public void deleteCenter(Integer id) {
		logger.debug("Deleting Center with id= {}", id);
		centerRepository.delete(id);
	}

	@Override
	public boolean existsCenterById(Integer id) {
		logger.debug("Exists or not Center with id {}", id);
		return centerRepository.exists(id);
	}

	@Override
	public Center saveCenter(Center center) {
		logger.debug("Updating Center {}", center);
		return centerRepository.save(center);
	}

	@Override
	public List<Center> findAllCenters() {
		logger.debug("Finding All Centers {}");
		return centerRepository.findAll();
	}

	@Override
	public List<Center> findAllCenters(int size, int first, String sort, String field) {
		List<Center> lista = PaginationUtil.convertCenterPageToList(
				centerRepository.findAll(PaginationUtil.createPageRequest(first, size, sort, field)));
		return lista;
	}

	@Override
	public Center insertCenter(Center center) {
		logger.debug("Inserting Center {}", center);
		return centerRepository.insert(center);
	}

}
