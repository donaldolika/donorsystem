package com.enterprise.service;

import java.util.List;
import com.enterprise.entity.Donation;

public interface DonationService {

	public Donation findOneDonationById(Long id);

	public void deleteDonation(Donation donation);

	public void deleteDonation(Long id);

	public boolean existsDonationById(Long id);

	public Donation saveDonation(Donation donation);

	public List<Donation> findAllDonation();
	
	
	public List<Donation> findAllDonation(int size,int first,String sort,String field);

	public Donation insertDonation(Donation donation);
}
