package com.enterprise.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enterprise.entity.Donor;
import com.enterprise.repository.DonorRepository;
import com.enterprise.utilities.PaginationUtil;

@Service
public class DonorServiceImpl implements DonorService, Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private DonorRepository donorRepository;

	@Override
	public List<Donor> findAllDonors(int size, int first, String sort, String field) {
		List<Donor> lista = PaginationUtil.convertDonorPageToList(
				donorRepository.findAll(PaginationUtil.createPageRequest(first, size, sort, field)));
		return lista;
	}

	@Override
	public void deleteDonor(Donor donor) {
		donorRepository.delete(donor);
	}

	@Override
	public void deleteDonor(Long id) {
		donorRepository.delete(id);
	}

	@Override
	public boolean existsDonors(Long id) {
		return donorRepository.exists(id);
	}

	@Override
	public Donor saveDonor(Donor donor) {
		return donorRepository.save(donor);
	}

	@Override
	public List<Donor> findAllDonors() {
		return donorRepository.findAll();
	}

	@Override
	public Donor insertDonors(Donor donor) {
		return donorRepository.insert(donor);
	}

	@Override
	public Donor findOneDonorById(Long id) {
		return donorRepository.findOne(id);
	}

}
