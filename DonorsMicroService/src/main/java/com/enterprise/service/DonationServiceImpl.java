package com.enterprise.service;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.enterprise.entity.Donation;
import com.enterprise.repository.DonationRepository;
import com.enterprise.utilities.PaginationUtil;

@Service
public class DonationServiceImpl implements DonationService, Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private DonationRepository donationRepository;

	@Override
	public Donation findOneDonationById(Long id) {
		return donationRepository.findOne(id);
	}

	@Override
	public List<Donation> findAllDonation(int size, int first, String sort, String field) {
		List<Donation> lista = PaginationUtil.convertDonationPageToList(
				donationRepository.findAll(PaginationUtil.createPageRequest(first, size, sort, field)));
		return lista;
	}

	@Override
	public void deleteDonation(Donation donation) {
		donationRepository.delete(donation);

	}

	@Override
	public void deleteDonation(Long id) {
		donationRepository.delete(id);

	}

	@Override
	public boolean existsDonationById(Long id) {
		return donationRepository.exists(id);
	}

	@Override
	public Donation saveDonation(Donation donation) {
		return donationRepository.save(donation);
	}

	@Override
	public List<Donation> findAllDonation() {

		return donationRepository.findAll();
	}

	@Override
	public Donation insertDonation(Donation donation) {

		return donationRepository.insert(donation);
	}

}
