package com.enterprise.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.enterprise.domain.User;


@Repository
public interface UserRepository extends CrudRepository<User, String> {

}
