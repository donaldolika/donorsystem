package com.enterprise.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.InMemoryAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import com.enterprise.service.UserService;

@Configuration
@EnableAuthorizationServer
public class OAuthConf extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager authenticationManager;

	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Autowired
	private DataSource dataSource;

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private UserService userService;

	@Autowired
	private Environment env;

	@Bean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}

	@Bean
	protected AuthorizationCodeServices authorizationCodeServices() {
		return new InMemoryAuthorizationCodeServices();
	}

	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService);
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.tokenStore(tokenStore)
									.authenticationManager(authenticationManager)
											.userDetailsService(userService);
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
	}

	/**
	 * Setup the client application which attempts to get access to user's
	 * account after user permission.
	 */
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
				.withClient("portal")
				.secret("portalsecret")
				.authorizedGrantTypes("refresh_token", "password")
				.scopes("ui")		
		 .and()
				.withClient("mobile")
				.authorizedGrantTypes("refresh_token", "password")
				.scopes("ui")
		.and()
		
				.withClient("ApiGateway")
				.secret(env.getProperty("GATEWAY_PASSWORD"))
				.authorizedGrantTypes("client_credentials", "refresh_token")
				.scopes("server")
		.and()
		
				.withClient("AccountMicroService")
				.secret(env.getProperty("USER_SERVICE_PASSWORD"))
				.authorizedGrantTypes("client_credentials", "refresh_token")
				.scopes("server")
		.and()
				.withClient("DonorsMicroService")
				.secret(env.getProperty("DONORS_SERVICE_PASSWORD"))
				.authorizedGrantTypes("client_credentials", "refresh_token")
				.scopes("server")
		.and()
				.withClient("NotificationMicroService")
				.secret(env.getProperty("NOTIFICATION_SERVICE_PASSWORD"))
				.authorizedGrantTypes("client_credentials", "refresh_token")
				.scopes("server")
		.and()
				.withClient("DataProcessingMicroService")
				.secret(env.getProperty("PROCESSING_SERVICE_PASSWORD"))
				.authorizedGrantTypes("client_credentials", "refresh_token")
				.scopes("server")
		.and()
				.withClient("NotificationMicroService")
				.secret(env.getProperty("NOTIFICATION_SERVICE_PASSWORD"))
				.authorizedGrantTypes("client_credentials", "refresh_token")
				.scopes("server")
		.and()
				.withClient("NotificationMicroService")
				.secret(env.getProperty("NOTIFICATION_SERVICE_PASSWORD"))
				.authorizedGrantTypes("client_credentials", "refresh_token")
				.scopes("server");

	}

}
